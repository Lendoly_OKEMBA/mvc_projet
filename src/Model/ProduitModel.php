<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProduitModel extends AbstractModel{
    protected static $table = 'produits';


    public static function getTable()
    {
        return self::$table;
    }

    private $id;

    public function getId()
    {
        return $this->id;
    }

    private $titre;

    public function getTitre()
    {
        return $this->titre;
    }

    private $reference;

    public function getReference()
    {
        return $this->reference;
    }

    private $description;

    public function getDescription()
    {
        return $this->description;
    }

    private $date_end;

    public function getDateEnd()
    {
        return $this->date_end;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . "(titre, reference, description) VALUES (?,?,?)",
            array($post['titre'], $post['reference'], $post['description'])
        );
    }


    public static function update($id,$post){
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ? WHERE id = ?",
            array($post['titre'], $post['reference'], $post['description'], $id)
        );
    }


}