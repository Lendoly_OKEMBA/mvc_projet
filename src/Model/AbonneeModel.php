<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AbonneeModel extends AbstractModel{
    protected static $table = 'abonnee';


    private $id;

    public function getId()
    {
        return $this->id;
    }

    private $prenom;

    public function getPrenom()
    {
        return $this->prenom;
    }

    private $nom;

    public function getNom()
    {
        return $this->nom;
    }

    private $email;

    public function getEmail()
    {
        return $this->email;
    }

    private $age;

    public function getAge()
    {
        return $this->age;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . "(nom, prenom, email, age) VALUES (?,?,?,?)",
            array($post['nom'], $post['prenom'], $post['email'], $post['age'])
        );
    }


    public static function update($id,$post){
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET nom = ?, prenom = ?, email = ?, age = ? WHERE id = ?",
            array($post['nom'], $post['prenom'], $post['email'], $post['age'], $id)
        );
    }

    public static function getAllAbonneeOrderBy($column = 'nom', $order ='ASC'){
        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
    }

    public static function getUserByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE email = ?",array($email),get_called_class(), true);
    }

}