<?php
namespace App\Controller;

use App\Model\AbonneeModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;

class AbonneController extends AbstractController{
    public function index(){
        $abonnees = AbonneeModel::all();
        $this->render('app.abonnee.abonnee', [
            'abonnees' => $abonnees,
        ], 'admin');
    }

    public function single($id){
        $abonnees = AbonneeModel::findById($id);
        $this->render('app.abonnee.single-ab', [
            'abonnees' => $abonnees,
        ], 'admin');
    }

    public function add(){
        $errors = [];
        if (!empty($_POST['submitted'])){
            //Faille XSS
            $post = $this->cleanXss($_POST);
            //Validation
            $validation = new Validation();
            $errors['nom'] = $validation->textValid($post['nom'], 'nom', 3, 20);
            $errors['prenom'] = $validation->textValid($post['prenom'], 'prenom', 3, 20);
            $errors['email'] = $validation->emailValid($post['email']);
            if ($validation->IsValid($errors)){
                $verifUser = AbonneeModel::getUserByEmail($post['email']);
                if(!empty($verifUser)) {
                    $errors['email'] = 'Votre compte existe dèjà';
                }
                if ($validation->IsValid($errors)){
                    AbonneeModel::insert($post);
                    //Message Flash
                    $this->addFlash('success', 'Abonnée ajouté !');
                    //Redirection
                    $this->redirect('abonnee');
                }
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnee.add-ab',[
            'form' => $form,
            'abonnees' => AbonneeModel::all(),
        ], 'admin');
    }

    public function edit($id){
        $errors = [];
        if (!empty($_POST['submitted'])){
            // Faille XSS
            $post =$this->cleanXss($_POST);
            //Validation
            $validation = new Validation();
            $errors['nom'] = $validation->textValid($post['nom'], 'nom', 3, 20);
            $errors['prenom'] = $validation->textValid($post['nom'], 'prenom', 3, 20);
            $errors['email'] = $validation->emailValid($post['email']);
            if ($validation->IsValid($errors)){
                AbonneeModel::update($id, $post);
                //Message Flash
                $this->addFlash('success', 'Edit effectué !');
                //Redirection
                $this->redirect('abonnee');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnee.edit-ab',[
            'form' => $form,
            'abonnees' => AbonneeModel::all(),
        ], 'admin');
    }

    public function delete($id){
        $abonnee = AbonneeModel::findById($id);
        if(empty($abonnee)) {
            $this->Abort404();
        }else{
            AbonneeModel::delete($id);
        }
        $this->addFlash('success', 'Abonné supprimé !');
        $this->render('app.abonnee.abonnee',[
            'abonnee' => $abonnee,
        ]);
        $this->redirect('abonnee');
    }
}