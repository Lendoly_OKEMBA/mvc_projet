<?php
namespace App\Controller;


use App\Model\ProduitModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;

class ProduitController extends AbstractController{
        public function index(){
            $produits = ProduitModel::all();
            $this->render('app.produits.produits',[
                'produits' => $produits,
            ]);
        }

        public function single($id){
            $produits = ProduitModel::findById($id);
            $this->render('app.produits.single-p', [
                'produits' => $produits,
            ], 'admin');
        }

        public function add(){
            $errors = [];
            if (!empty($_POST['submitted'])){
                //Faille XSS
                $post = $this->cleanXss($_POST);
                //Validation
                $validation = new Validation();
                $errors['titre'] = $validation->textValid($post['titre'], 'titre', 3, 20);
                $errors['reference'] = $validation->textValid($post['reference'], 'reference', 3, 20);
                $errors['description'] = $validation->textValid($post['description'], 'description', 3, 20);
                if ($validation->IsValid($errors)){
                    ProduitModel::insert($post);
                    //Message flash
                    $this->addFlash('succes', 'Nouveau produit ajouté');
                    //Redirection
                    $this->redirect('produits');
                }
            }
            $form = new Form($errors);
            $this->render('app.produits.add-p',[
                'form' => $form,
            ]);
        }

        public function edit($id){
            $errors = [];
            if (!empty($_POST['submitted'])){
                //Faille XSS
                $post = $this->cleanXss($_POST);
                //Validation
                $validation = new Validation();
                $errors['titre'] = $validation->textValid($post['titre'], 'titre', 3, 20);
                $errors['reference'] = $validation->textValid($post['reference'], 'reference', 3, 20);
                $errors['description'] = $validation->textValid($post['description'], 'description', 3, 30);
                if ($validation->IsValid($errors)){
                    ProduitModel::update($id, $post);
                    //Message flash
                    $this->addFlash('success', 'Produit édité');
                    //Redirection
                    $this->redirect('produits');
                }
            }
            $form = new Form($errors);
            $this->render('app.produits.edit-p',[
                'form' => $form,
            ]);
        }

        public function delete($id){
            $produit = ProduitModel::findById($id);
            if(empty($produit)) {
                $this->Abort404();
            }else{
                ProduitModel::delete($id);
            }
            $this->addFlash('success', 'Abonné supprimé !');
            $this->render('app.produits.produits',[
                'produit' => $produit,
            ]);
            $this->redirect('produit');
        }
}