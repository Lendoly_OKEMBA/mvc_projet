<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<header id="masthead">
    <nav>
        <ul>
            <li><a href="<?= $view->path(''); ?>">Home</a></li>
            <li><a href="<?= $view->path(''); ?>">abonnee</a></li>
            <li><a href="<?= $view->path('add-ab'); ?>">Ajouter un abonnée</a></li>
            <li><a href="<?= $view->path('produits'); ?>">Produits</a></li>
            <li><a href="<?= $view->path('add-p'); ?>">Ajouter un produit</a></li>
        </ul>
    </nav>
</header>

<section id="sidebar">
    <nav class="sidebar">
        <div class="container">
            <div class="img_texte">
                  <span class="image">
                      <img src="" alt="">
                  </span>

                <div class="text header-text">
                    <span class="name"></span>
                    <span class="profession"></span>
                </div>
            </div>
        </div>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">
                    <li class="nav-link">
                        <a href="<?= $view->path('abonnee'); ?>">
                              <span class="text">
                                    abonnée
                              </span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="<?= $view->path('abonnee'); ?>">
                              <span class="text">
                                    abonnée
                              </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>

<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div class="wrap">
        <p>MVC 6 - Admin</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
