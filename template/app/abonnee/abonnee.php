<h1 style="text-align: center; margin: 100px 0">
    <ul>
        <?php
        foreach ($abonnees as $abonnee){
            echo
                '<div>
                 <h2>'.$abonnee->prenom.'</h2>
                 <a href="'.$view->path('single-ab', ['id' => $abonnee->id]).'">Détails</a>
                 <a href="'.$view->path('edit-ab', ['id' => $abonnee->id]).'">Editer</a>
                 <a onclick="return confirm(\'Voulez-vous effacer ?\')" href="'.$view->path('delete-ab', ['id' => $abonnee->id]).'">Effacer</a>
                </div>';
        }
        ?>
    </ul>
</h1>