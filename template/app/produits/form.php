<form action="" method="post" novalidate style="display: flex; align-items: center; flex-direction: column ">
    <?php echo $form->label('titre');?>
    <?php echo $form->input('titre', 'text', $produits->titre ?? '');?>
    <?php echo $form->error('titre');?>

    <?php echo $form->label('reference');?>
    <?php echo $form->input('reference','text', $produits->reference ?? '');?>
    <?php echo $form->error('reference');?>

    <?php echo $form->label('description');?>
    <?php echo $form->input('description', 'text', $produits->description ?? '');?>
    <?php echo $form->error('description');?>

    <?php echo $form->submit('submitted', $textButton);?>
</form>