<h1 style="text-align: center; margin: 100px 0">
    <ul>
        <?php
        foreach ($produits as $produit){
            echo
                '<div>
                 <h2>'.$produit->titre.'</h2>
                 <a href="'.$view->path('single-p', ['id' => $produit->id]).'">Détails</a>
                 <a href="'.$view->path('edit-p', ['id' => $produit->id]).'">Editer</a>
                 <a onclick="return confirm(\'Voulez-vous effacer ?\')" href="'.$view->path('delete-p', ['id' => $produit->id]).'">Effacer</a>
                </div>';
        }
        ?>
    </ul>
</h1>