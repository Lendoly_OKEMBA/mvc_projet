<?php

$routes = array(
    array('home','default','index'),

    ['abonnee', 'abonne', 'index'],
    ['add-ab', 'abonne', 'add'],
    ['edit-ab', 'abonne', 'edit', ['id']],
    ['single-ab', 'abonne', 'single', ['id']],
    ['delete-ab', 'abonne', 'delete', ['id']],

    ['produits', 'produit', 'index'],
    ['add-p', 'produit', 'add'],
    ['single-p', 'produit', 'single',['id']],
    ['edit-p', 'produit', 'edit',['id']],
    ['delete-p', 'produit', 'delete', ['id']],
);









